package rodrigo.petter.dpebancodehoras.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import rodrigo.petter.dpebancodehoras.model.entity.User
import rodrigo.petter.dpebancodehoras.model.repository.UserRepository

import javax.validation.Valid

@Controller
@RequestMapping(path = "user")
class UserController {

    @Autowired
    UserRepository repository

    @GetMapping(path = '/new')
    String novo() {
        return 'user/new'
    }

    @PostMapping(path = '/new')
    String inserir(@Valid @ModelAttribute User usuario) {
        return 'login'
    }

}