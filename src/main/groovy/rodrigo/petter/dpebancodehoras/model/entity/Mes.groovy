package rodrigo.petter.dpebancodehoras.model.entity

import groovy.transform.Canonical

import java.time.Month

@Canonical
class Mes {

    Integer ano
    Month mes

    Dia[] dias

}
