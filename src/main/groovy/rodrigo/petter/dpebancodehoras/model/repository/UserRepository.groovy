package rodrigo.petter.dpebancodehoras.model.repository

import org.springframework.data.jpa.repository.JpaRepository
import rodrigo.petter.dpebancodehoras.model.entity.User

interface UserRepository extends JpaRepository<User, Integer> {

    User findByMatricula(Integer matricula)

}
