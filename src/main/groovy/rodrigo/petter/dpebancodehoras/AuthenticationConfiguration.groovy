package rodrigo.petter.dpebancodehoras

import org.springframework.context.annotation.*
import org.springframework.security.config.annotation.web.configuration.*
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.provisioning.InMemoryUserDetailsManager
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@EnableWebSecurity
class AuthenticationConfiguration implements WebMvcConfigurer {

    @Bean
    UserDetailsService userDetailsService() throws Exception {
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager()
        manager.createUser(User.withDefaultPasswordEncoder().username("user").password("user").roles("USER").build())
        return manager
    }
}