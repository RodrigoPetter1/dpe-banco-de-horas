package rodrigo.petter.dpebancodehoras.model.entity

import groovy.transform.Canonical

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.NotNull

@Entity(name = "usuario")
@Canonical
class User {

    @Id()
    @Column()
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id

    @Column(unique = true)
    @NotNull
    Integer matricula

    @Column
    @NotNull
    String nome

    @Column
    @NotNull
    String senha

    @Column
    @NotNull
    String email

    @Column
    @NotNull
    String lotacao

    @Column
    @NotNull
    String cargo
}
