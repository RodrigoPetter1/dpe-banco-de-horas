package rodrigo.petter.dpebancodehoras.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class LoginController {

    @GetMapping(path = '/login')
    String login() {
        return 'login'
    }
}
