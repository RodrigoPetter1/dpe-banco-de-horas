package rodrigo.petter.dpebancodehoras

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class DpeBancoDeHorasApplication {

    static void main(String[] args) {
        SpringApplication.run DpeBancoDeHorasApplication, args
    }
}
